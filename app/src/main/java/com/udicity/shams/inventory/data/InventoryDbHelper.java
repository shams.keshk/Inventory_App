package com.udicity.shams.inventory.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.udicity.shams.inventory.data.InventoryContract.InventoryEntry;

/**
 * Created by Shams_Keshk on 12/09/17.
 */

public class InventoryDbHelper extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 1;

    public static final String DATABASE_NAME = "inventory.db";

    private static final String SQL_CREATE_ENTRIES = "CREATE TABLE " + InventoryEntry.TABLE_NAME + "(" +
            InventoryEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT ," +
            InventoryEntry.COLUMN_PRODUCT_NAME + " TEXT NOT NULL ," +
            InventoryEntry.COLUMN_PRODUCT_QUANTITY + " INTEGER NOT NULL DEFAULT 0 ," +
            InventoryEntry.COLUMN_PRODUCT_QUANTITY_TYPE + " INTEGER NOT NULL DEFAULT 0 ," +
            InventoryEntry.COLUMN_PRODUCT_PRICE + " INTEGER NOT NULL DEFAULT 0 ," +
            InventoryEntry.COLUMN_PRODUCT_PRICE_TYPE + " INTEGER NOT NULL DEFAULT 0 , " +
            InventoryEntry.COLUMN_PRODUCT_SUPPLIER_NAME + " TEXT NOT NULL ," +
            InventoryEntry.COLUMN_PRODUCT_SUPPLIER_EMAIL + " TEXT NOT NULL ," +
            InventoryEntry.COLUMN_PRODUCT_SUPPLIER_PHONE + " TEXT NOT NULL ," +
            InventoryEntry.COLUMN_PRODUCT_SUPPLIER_ADDRESS + " TEXT ," +
            InventoryEntry.COLUMN_PRODUCT_IMAGE + " TEXT NOT NULL ) ;";

    private static final String SQL_DELETE_ENTRIES = "DROP TABLE IF EXISTS " + InventoryEntry.TABLE_NAME;


    public InventoryDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(SQL_CREATE_ENTRIES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL(SQL_DELETE_ENTRIES);
        onCreate(sqLiteDatabase);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }
}
