package com.udicity.shams.inventory;

import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.Toast;

import com.udicity.shams.inventory.data.InventoryContract.InventoryEntry;

/**
 * Created by Shams_Keshk on 12/09/17.
 */

public class InventoryCursorAdapter extends CursorAdapter {

    public InventoryCursorAdapter(Context context, Cursor cursor) {
        super(context, cursor, 0);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
        View v = LayoutInflater.from(context).inflate(R.layout.activity_main_inventory_item, viewGroup, false);
        InventoryViewHolder viewHolder = new InventoryViewHolder(v);
        v.setTag(viewHolder);
        return v;
    }

    @Override
    public void bindView(View view, final Context context, Cursor cursor) {

        final InventoryViewHolder viewHolder = (InventoryViewHolder) view.getTag();

        //get product name and set it in View
        String productName = cursor.getString(cursor.getColumnIndexOrThrow(InventoryEntry.COLUMN_PRODUCT_NAME));
        viewHolder.productNameView.setText(productName);

        //Get Quantity Value and set The QuantityView
        final int productQuantity = cursor.getInt(cursor.getColumnIndexOrThrow(InventoryEntry.COLUMN_PRODUCT_QUANTITY));
        viewHolder.productQuantityView.setText(String.valueOf(productQuantity));

        //Get The Quantity Type
        int productQuantityType = cursor.getInt(cursor.getColumnIndexOrThrow(InventoryEntry.COLUMN_PRODUCT_QUANTITY_TYPE));
        switch (productQuantityType) {
            case 1:
                viewHolder.productQuantityTypeView.setText(context.getString(R.string.quantity_type_kg));
                break;
            default:
                viewHolder.productQuantityTypeView.setText(context.getString(R.string.quantity_type_piece));
                break;
        }

        //Get Price and set The View
        int productPrice = cursor.getInt(cursor.getColumnIndexOrThrow(InventoryEntry.COLUMN_PRODUCT_PRICE));
        viewHolder.productPriceView.setText(String.valueOf(productPrice));

        //Get The Price Type
        int productPriceType = cursor.getInt(cursor.getColumnIndexOrThrow(InventoryEntry.COLUMN_PRODUCT_PRICE_TYPE));
        switch (productPriceType) {
            case 1:
                viewHolder.productPriceTypeView.setText(context.getString(R.string.price_type_euro));

                break;
            case 2:
                viewHolder.productPriceTypeView.setText(context.getString(R.string.price_type_kwd));

                break;
            case 3:
                viewHolder.productPriceTypeView.setText(context.getString(R.string.price_type_gbp));

                break;
            case 4:
                viewHolder.productPriceTypeView.setText(context.getString(R.string.price_type_swiss_franc));

                break;
            case 5:
                viewHolder.productPriceTypeView.setText(context.getString(R.string.price_type_yen));

                break;
            default:
                viewHolder.productPriceTypeView.setText(context.getString(R.string.price_type_usd));
                break;
        }

        //Get Supplier Name And Set The View
        String supplierName = cursor.getString(cursor.getColumnIndexOrThrow(InventoryEntry.COLUMN_PRODUCT_SUPPLIER_NAME));
        viewHolder.supplierNameView.setText(supplierName);

        //Get The Position Of The Current Product "PRODUCT ID"
        final int position = cursor.getInt(cursor.getColumnIndexOrThrow(InventoryEntry._ID));

        //This Button Sale The Quantity Number Of Product
        viewHolder.saleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (productQuantity == 1) {
                    Toast.makeText(context, context.getString(R.string.minimum_value_of_product_quantity_at_least_one), Toast.LENGTH_SHORT).show();
                } else {
                    Uri updatedUri = ContentUris.withAppendedId(InventoryEntry.CONTENT_URI, position);

                    ContentValues values = new ContentValues();
                    //When Click On Sale Button THe Quantity Reduced By One
                    values.put(InventoryEntry.COLUMN_PRODUCT_QUANTITY, productQuantity - 1);

                    context.getContentResolver().update(updatedUri, values, null, null);
                }
            }
        });

        //get image Url From DB And Pass It To Bitmap to set The Image View
        final Uri imageUri = Uri.parse(cursor.getString(cursor.getColumnIndexOrThrow(InventoryEntry.COLUMN_PRODUCT_IMAGE)));

        ViewTreeObserver viewTreeObserver = viewHolder.productImageView.getViewTreeObserver();
        viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                viewHolder.productImageView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                viewHolder.productImageView.setImageBitmap(UploadImageBitmap.getBitmapFromUri(imageUri, context,viewHolder.productImageView));
            }
        });
        //viewHolder.productImageView.setImageBitmap(UploadImageBitmap.convertImageUriByBitmap(imageUri, context));

    }


}
