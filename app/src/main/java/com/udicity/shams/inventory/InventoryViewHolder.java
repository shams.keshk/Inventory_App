package com.udicity.shams.inventory;

import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Shams_Keshk on 12/09/17.
 */

public class InventoryViewHolder {

    @BindView(R.id.inventory_item_name_id)
    TextView productNameView;
    @BindView(R.id.inventory_item_quantity_id)
    TextView productQuantityView;
    @BindView(R.id.inventory_item_quantity_type_id)
    TextView productQuantityTypeView;
    @BindView(R.id.inventory_item_price_id)
    TextView productPriceView;
    @BindView(R.id.inventory_item_price_type_id)
    TextView productPriceTypeView;
    @BindView(R.id.inventory_item_supplier_name_id)
    TextView supplierNameView;

    @BindView(R.id.inventory_image_item_id)
    ImageView productImageView;

    @BindView(R.id.inventory_item_sale_quantity_button_id)
    Button saleButton;

    public InventoryViewHolder(View view) {
        ButterKnife.bind(this, view);
    }
}
