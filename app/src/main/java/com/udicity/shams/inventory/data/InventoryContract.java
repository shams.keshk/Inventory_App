package com.udicity.shams.inventory.data;

import android.content.ContentResolver;
import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Created by Shams_Keshk on 12/09/17.
 */

public class InventoryContract {

    public static final String CONTENT_AUTHORITY = "com.udicity.shams.inventory";

    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);

    public static final String INVENTORY_PATH = "inventory";

    private InventoryContract() {

    }

    public static final class InventoryEntry implements BaseColumns {

        public static final Uri CONTENT_URI = Uri.withAppendedPath(BASE_CONTENT_URI, INVENTORY_PATH);

        public static final String CONTENT_LIST_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + INVENTORY_PATH;

        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + INVENTORY_PATH;

        public static final String TABLE_NAME = "inventory";

        // Table Columns
        public static final String _ID = BaseColumns._ID;

        public static final String COLUMN_PRODUCT_NAME = "product_name";

        public static final String COLUMN_PRODUCT_QUANTITY = "quantity";

        public static final String COLUMN_PRODUCT_QUANTITY_TYPE = "quantity_type";

        public static final String COLUMN_PRODUCT_PRICE = "price";

        public static final String COLUMN_PRODUCT_PRICE_TYPE = "price_type";

        public static final String COLUMN_PRODUCT_SUPPLIER_NAME = "supplier_name";

        public static final String COLUMN_PRODUCT_SUPPLIER_EMAIL = "supplier_email";

        public static final String COLUMN_PRODUCT_SUPPLIER_PHONE = "supplier_phone";

        public static final String COLUMN_PRODUCT_SUPPLIER_ADDRESS = "supplier_address";

        public static final String COLUMN_PRODUCT_IMAGE = "product_image";

        //Constants Value Of Product Quantity Types
        public static final int QUANTITY_TYPE_PEACE = 0;

        public static final int QUANTITY_TYPE_KG = 1;

        //Constant Values Of Price Types
        public static final int PRICE_TYPE_USD = 0;
        public static final int PRICE_TYPE_EURO = 1;
        public static final int PRICE_TYPE_KWD = 2;
        public static final int PRICE_TYPE_GBP = 3;
        public static final int PRICE_TYPE_SWISS_FRANK = 4;
        public static final int PRICE_TYPE_YEN = 5;

    }
}
