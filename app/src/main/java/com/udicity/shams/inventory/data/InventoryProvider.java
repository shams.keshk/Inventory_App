package com.udicity.shams.inventory.data;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;

import com.udicity.shams.inventory.R;
import com.udicity.shams.inventory.data.InventoryContract.InventoryEntry;


/**
 * Created by Shams_Keshk on 12/09/17.
 */

public class InventoryProvider extends ContentProvider {

    private static final String LOG_TAG = InventoryProvider.class.getSimpleName();

    private static final int INVENTORY_ITEMS = 10;

    private static final int INVENTORY_ITEM_ID = 20;

    private static final UriMatcher uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);

    static {
        uriMatcher.addURI(InventoryContract.CONTENT_AUTHORITY, InventoryContract.INVENTORY_PATH, INVENTORY_ITEMS);

        uriMatcher.addURI(InventoryContract.CONTENT_AUTHORITY, InventoryContract.INVENTORY_PATH + "/#", INVENTORY_ITEM_ID);
    }

    private InventoryDbHelper inventoryDbHelper;

    @Override
    public boolean onCreate() {
        inventoryDbHelper = new InventoryDbHelper(getContext());

        return true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sort) {
        SQLiteDatabase sqLiteDatabase = inventoryDbHelper.getReadableDatabase();

        Cursor cursor;

        int matchUri = uriMatcher.match(uri);
        switch (matchUri) {
            case INVENTORY_ITEMS:
                cursor = sqLiteDatabase.query(InventoryEntry.TABLE_NAME, projection, selection, selectionArgs, null, null, sort);
                break;
            case INVENTORY_ITEM_ID:
                selection = InventoryEntry._ID + "=?";
                selectionArgs = new String[]{String.valueOf(ContentUris.parseId(uri))};

                cursor = sqLiteDatabase.query(InventoryEntry.TABLE_NAME, projection, selection, selectionArgs, null, null, sort);

                break;
            default:
                throw new IllegalArgumentException(getContext().getString(R.string.can_not_query_for_invalid_uri) + uri);
        }

        cursor.setNotificationUri(getContext().getContentResolver(), uri);

        return cursor;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues contentValues) {

        int matchUri = uriMatcher.match(uri);
        switch (matchUri) {
            case INVENTORY_ITEMS:
                return insertValues(uri, contentValues);
            default:
                throw new IllegalArgumentException(getContext().getString(R.string.can_not_insert_values_for_invalid_uri) + uri);
        }
    }

    private Uri insertValues(Uri uri, ContentValues contentValues) {
        validateInsertValues(contentValues);

        SQLiteDatabase sqLiteDatabase = inventoryDbHelper.getWritableDatabase();

        long id = sqLiteDatabase.insert(InventoryEntry.TABLE_NAME, null, contentValues);

        if (id == -1) {
            Log.e(LOG_TAG, getContext().getString(R.string.failed_to_insert_row) + uri);
            return null;
        }

        getContext().getContentResolver().notifyChange(uri, null);

        return ContentUris.withAppendedId(uri, id);
    }


    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {

        SQLiteDatabase sqLiteDatabase = inventoryDbHelper.getWritableDatabase();
        int deletedRowId;
        int matchUri = uriMatcher.match(uri);
        switch (matchUri) {
            case INVENTORY_ITEMS:
                deletedRowId = sqLiteDatabase.delete(InventoryEntry.TABLE_NAME, selection, selectionArgs);

                if (deletedRowId != 0) {
                    getContext().getContentResolver().notifyChange(uri, null);
                }
                return deletedRowId;
            case INVENTORY_ITEM_ID:
                selection = InventoryEntry._ID + "=?";
                selectionArgs = new String[]{String.valueOf(ContentUris.parseId(uri))};
                deletedRowId = sqLiteDatabase.delete(InventoryEntry.TABLE_NAME, selection, selectionArgs);

                if (deletedRowId != 0) {
                    getContext().getContentResolver().notifyChange(uri, null);
                }

                return deletedRowId;
            default:
                throw new IllegalArgumentException(getContext().getString(R.string.can_cot_delete_values_of_invalid_uri) + uri);
        }
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues contentValues, @Nullable String selection, @Nullable String[] selectionArgs) {
        int matchUri = uriMatcher.match(uri);
        switch (matchUri) {
            case INVENTORY_ITEMS:
                return updateValues(uri, contentValues, selection, selectionArgs);
            case INVENTORY_ITEM_ID:
                selection = InventoryEntry._ID + "=?";
                selectionArgs = new String[]{String.valueOf(ContentUris.parseId(uri))};
                return updateValues(uri, contentValues, selection, selectionArgs);
            default:
                throw new IllegalArgumentException(getContext().getString(R.string.can_cot_update_values_for_invalid_uri) + uri);
        }
    }

    private int updateValues(Uri uri, ContentValues contentValues, String selection, String[] selectionArgs) {
        if (contentValues.size() == 0) {
            return 0;
        }
        validateUpdateValues(contentValues);

        SQLiteDatabase sqLiteDatabase = inventoryDbHelper.getWritableDatabase();

        int rowId = sqLiteDatabase.update(InventoryEntry.TABLE_NAME, contentValues, selection, selectionArgs);

        if (rowId != 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }

        return rowId;

    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        int matchUri = uriMatcher.match(uri);
        switch (matchUri) {
            case INVENTORY_ITEMS:
                return InventoryEntry.CONTENT_LIST_TYPE;
            case INVENTORY_ITEM_ID:
                return InventoryEntry.CONTENT_ITEM_TYPE;
            default:
                throw new IllegalStateException("Un Known Uri : " + uri + " That Match : " + matchUri);
        }
    }

    private void validateUpdateValues(ContentValues values) {

        if (values.containsKey(InventoryEntry.COLUMN_PRODUCT_NAME)) {
            String productName = values.getAsString(InventoryEntry.COLUMN_PRODUCT_NAME);
            validateStringValues(productName, getContext().getString(R.string.product_require_name));
        }

        if (values.containsKey(InventoryEntry.COLUMN_PRODUCT_QUANTITY)) {
            Integer productQuantity = values.getAsInteger(InventoryEntry.COLUMN_PRODUCT_QUANTITY);
            validateIntegerValues(productQuantity, getContext().getString(R.string.product_quantity_has_invalid_value));
        }

        if (values.containsKey(InventoryEntry.COLUMN_PRODUCT_PRICE)) {
            Integer productPrice = values.getAsInteger(InventoryEntry.COLUMN_PRODUCT_PRICE);
            validateIntegerValues(productPrice, getContext().getString(R.string.product_price_has_invalid_value));
        }

        if (values.containsKey(InventoryEntry.COLUMN_PRODUCT_SUPPLIER_NAME)) {
            String supplierName = values.getAsString(InventoryEntry.COLUMN_PRODUCT_SUPPLIER_NAME);
            validateStringValues(supplierName, getContext().getString(R.string.supplier_require_name));
        }

        if (values.containsKey(InventoryEntry.COLUMN_PRODUCT_SUPPLIER_EMAIL)) {
            String supplierEmail = values.getAsString(InventoryEntry.COLUMN_PRODUCT_SUPPLIER_EMAIL);
            validateStringValues(supplierEmail, getContext().getString(R.string.supplier_require_email));
        }

        if (values.containsKey(InventoryEntry.COLUMN_PRODUCT_SUPPLIER_PHONE)) {
            String supplierPhone = values.getAsString(InventoryEntry.COLUMN_PRODUCT_SUPPLIER_PHONE);
            validateStringValues(supplierPhone, getContext().getString(R.string.supplier_require_phone));
        }

        //Address of Supplier can be null So we didn't validate it .

        if (values.containsKey(InventoryEntry.COLUMN_PRODUCT_IMAGE)) {
            String productImage = values.getAsString(InventoryEntry.COLUMN_PRODUCT_IMAGE);
            validateStringValues(productImage, getContext().getString(R.string.product_must_have_image));
        }

    }

    private void validateInsertValues(ContentValues values) {

        String productName = values.getAsString(InventoryEntry.COLUMN_PRODUCT_NAME);
        validateStringValues(productName, getContext().getString(R.string.product_require_name));

        Integer productQuantity = values.getAsInteger(InventoryEntry.COLUMN_PRODUCT_QUANTITY);
        validateIntegerValues(productQuantity, getContext().getString(R.string.product_quantity_has_invalid_value));

        Integer productPrice = values.getAsInteger(InventoryEntry.COLUMN_PRODUCT_PRICE);
        validateIntegerValues(productPrice, getContext().getString(R.string.product_price_has_invalid_value));

        String supplierName = values.getAsString(InventoryEntry.COLUMN_PRODUCT_SUPPLIER_NAME);
        validateStringValues(supplierName, getContext().getString(R.string.supplier_require_name));

        String supplierEmail = values.getAsString(InventoryEntry.COLUMN_PRODUCT_SUPPLIER_EMAIL);
        validateStringValues(supplierEmail, getContext().getString(R.string.supplier_require_email));

        String supplierPhone = values.getAsString(InventoryEntry.COLUMN_PRODUCT_SUPPLIER_PHONE);
        validateStringValues(supplierPhone, getContext().getString(R.string.supplier_require_phone));

        //Address of Supplier can be null So we didn't validate it .

        String productImage = values.getAsString(InventoryEntry.COLUMN_PRODUCT_IMAGE);
        validateStringValues(productImage, getContext().getString(R.string.product_must_have_image));
    }

    //Helper Methods

    private void validateStringValues(String stringValue, String errorMessage) {
        if (TextUtils.isEmpty(stringValue)) {
            throw new IllegalArgumentException(errorMessage);
        }
    }

    private void validateIntegerValues(Integer integerValue, String errorMessage) {
        if (integerValue == null || integerValue < 0) {
            throw new IllegalArgumentException(errorMessage);
        }
    }
}
