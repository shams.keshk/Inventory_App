package com.udicity.shams.inventory;

import android.content.ContentUris;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.udicity.shams.inventory.data.InventoryContract.InventoryEntry;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {

    private static final int INVENTORY_PRODUCT_LOADER = 1;
    @BindView(R.id.inventory_list_view)
    ListView inventoryListView;
    @BindView(R.id.empty_view)
    View emptyView;
    @BindView(R.id.fab_create_item)
    FloatingActionButton createItemFab;
    private InventoryCursorAdapter inventoryCursorAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        createItemFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, EditActivity.class);
                startActivity(intent);
            }
        });

        inventoryCursorAdapter = new InventoryCursorAdapter(this, null);

        inventoryListView.setEmptyView(emptyView);

        inventoryListView.setAdapter(inventoryCursorAdapter);

        inventoryListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Uri currentUri = ContentUris.withAppendedId(InventoryEntry.CONTENT_URI, id);
                Intent intent = new Intent(MainActivity.this, EditActivity.class);
                intent.setData(currentUri);
                startActivity(intent);
            }
        });

        getSupportLoaderManager().initLoader(INVENTORY_PRODUCT_LOADER, null, this);

    }


    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        String[] projection = {InventoryEntry._ID
                , InventoryEntry.COLUMN_PRODUCT_NAME
                , InventoryEntry.COLUMN_PRODUCT_QUANTITY
                , InventoryEntry.COLUMN_PRODUCT_QUANTITY_TYPE
                , InventoryEntry.COLUMN_PRODUCT_PRICE
                , InventoryEntry.COLUMN_PRODUCT_PRICE_TYPE
                , InventoryEntry.COLUMN_PRODUCT_SUPPLIER_NAME
                , InventoryEntry.COLUMN_PRODUCT_IMAGE};
        return new CursorLoader(this, InventoryEntry.CONTENT_URI, projection, null, null, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        inventoryCursorAdapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        inventoryCursorAdapter.swapCursor(null);
    }

    private void deleteProducts() {
        int rowsDeleted = 0;
        if (InventoryEntry.CONTENT_URI != null) {
            rowsDeleted = getContentResolver().delete(InventoryEntry.CONTENT_URI, null, null);
        }

        if (rowsDeleted == 0) {
            Toast.makeText(getApplicationContext(), getString(R.string.failed_to_delete_products_with_this_uri) + InventoryEntry.CONTENT_URI, Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(getApplicationContext(), getString(R.string.delete_products_done) + rowsDeleted, Toast.LENGTH_SHORT).show();
        }
    }

    private void showDeleteConformationDialog() {

        DialogInterface.OnClickListener positiveDialog = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                deleteProducts();
            }
        };

        DialogInterface.OnClickListener negativeDialog = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        };

        DisplayAlertDialog.showAlertDialog(this,
                getString(R.string.you_will_delete_all_products),
                getString(R.string.delete),
                positiveDialog,
                getString(R.string.cancel),
                negativeDialog);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_activity_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int iconId = item.getItemId();
        switch (iconId) {
            case R.id.delete_specific_item_icon_id:
                showDeleteConformationDialog();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
