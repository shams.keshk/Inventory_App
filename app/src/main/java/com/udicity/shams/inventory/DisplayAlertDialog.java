package com.udicity.shams.inventory;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;

/**
 * Created by Shams_Keshk on 15/09/17.
 */

public class DisplayAlertDialog {

    // Helper Method To Create Alert Catalog easily and faster

    public static void showAlertDialog(Context context, String message, String positiveTextAction, DialogInterface.OnClickListener positiveDialogInterface, String negativeTextAction, DialogInterface.OnClickListener negativeDialogInterface) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(message);
        builder.setPositiveButton(positiveTextAction, positiveDialogInterface);
        builder.setNegativeButton(negativeTextAction, negativeDialogInterface);

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

}
