package com.udicity.shams.inventory;

import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.LoaderManager;
import android.support.v4.app.NavUtils;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.udicity.shams.inventory.data.InventoryContract.InventoryEntry;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EditActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {

    public static final int PICK_PRODUCT_IMAGE = 1;
    private static final int EDITOR_ACTIVITY_LOADER = 2;

    @BindView(R.id.product_name_id)
    EditText productNameView;

    @BindView(R.id.product_quantity_id)
    EditText productQuantityView;

    @BindView(R.id.quantity_type_spinner_id)
    Spinner spinnerQuantityTypeView;

    @BindView(R.id.product_price_id)
    EditText productPriceView;

    @BindView(R.id.price_type_spinner_id)
    Spinner spinnerPriceTypeView;

    @BindView(R.id.product_supplier_name_id)
    EditText supplierNameView;

    @BindView(R.id.product_supplier_email_id)
    EditText supplierEmailView;

    @BindView(R.id.product_supplier_phone_id)
    EditText supplierPhoneView;

    @BindView(R.id.product_supplier_address_id)
    EditText supplierAddressView;

    @BindView(R.id.attach_image_floating_button_id)
    FloatingActionButton attachImageButton;

    @BindView(R.id.product_image_id)
    ImageView productImageView;

    @BindView(R.id.increment)
    Button incrementQuantity;

    @BindView(R.id.decrement)
    Button decrementQuantity;

    @BindView(R.id.order_product_button_id)
    Button orderButton;

    @BindView(R.id.save_button)
    Button saveProductButton;

    private String productName;
    private int productQuantity;
    private int productQuantityType;
    private int productPrice;
    private int productPriceType;
    private String supplierName;
    private String supplierEmail;
    private String supplierPhone;
    private String supplierAddress;
    private Uri currentUri;
    private Uri imageUri;

    private boolean productHasChanged;

    private View.OnTouchListener touchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            productHasChanged = true;
            return false;
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);
        ButterKnife.bind(this);

        /**
         * This Method {@link #viewsOnTouchListeners()}  Returns All Views in this Activity
         * That have onTouchListener {@link touchListener}
         * */
        viewsOnTouchListeners();

        //get the Current Uri Of THe Item That Clicked From product List
        currentUri = getIntent().getData();

        /*
         *if Uri Is Null so the User come from floating button
         *to create product the title will be Add Product
         */
        if (currentUri == null) {
            setTitle(getString(R.string.title_add_product));
        } else {
            setTitle(getString(R.string.title_edit_product));
            getSupportLoaderManager().initLoader(EDITOR_ACTIVITY_LOADER, null, this);
        }

        // Quantity Spinner Is Kg Or piece
        setUpQuantitySpinner();

        // Price Spinner Is USD Or Euro Or Yen etc
        setUpPriceSpinner();

        /**
         * This Method {@link #buttonListeners()}  Refer To All Buttons Listeners On This Activity Like :
         * {@link saveProductButton }
         * {@link orderButton }
         * {@link attachImageButton }
         * {@link incrementQuantity }
         * {@link decrementQuantity }
         * */
        buttonListeners();

    }

    private void viewsOnTouchListeners() {
        productNameView.setOnTouchListener(touchListener);
        incrementQuantity.setOnTouchListener(touchListener);
        productQuantityView.setOnTouchListener(touchListener);
        decrementQuantity.setOnTouchListener(touchListener);
        spinnerQuantityTypeView.setOnTouchListener(touchListener);
        productPriceView.setOnTouchListener(touchListener);
        spinnerPriceTypeView.setOnTouchListener(touchListener);
        supplierNameView.setOnTouchListener(touchListener);
        supplierEmailView.setOnTouchListener(touchListener);
        supplierPhoneView.setOnTouchListener(touchListener);
        supplierAddressView.setOnTouchListener(touchListener);
        attachImageButton.setOnTouchListener(touchListener);
    }

    private void buttonListeners() {
        attachImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uploadImageFromGallery();
            }
        });

        incrementQuantity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                productQuantity = productQuantity + 1;
                productQuantityView.setText(String.valueOf(productQuantity));
            }
        });

        decrementQuantity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (productQuantity != 0) {
                    productQuantity = productQuantity - 1;
                    productQuantityView.setText(String.valueOf(productQuantity));
                } else {
                    makeToast(getString(R.string.you_cant_set_quantity_less_than_zero));
                }
            }
        });

        orderButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getValuesFromViews();

                //First Start Make Validation For Values Before Send mail
                //the Important fields Only We Will Make Validation On It
                if (!validateName(productName)) {
                    validateNameMessages(productName);
                } else if (productQuantity == 0) {
                    makeToast(getString(R.string.quantity_cannot_be_zero));
                } else if (!validateName(supplierName)) {
                    validateSupplierNameMessages(supplierName);
                } else if (!validateSupplierEmail(supplierEmail)) {
                    validateEmailMessage(supplierEmail);
                } else if (imageUri == null) {
                    makeToast(getString(R.string.product_must_have_image));
                } else {
                    String subject = "Order From Inventory Corporation";
                    String orderDetails = "Dear " + supplierName + " :\n" +
                            "\t\tWe Hope Send This Order To Our Inventory as soon as possible :\n\n" +
                            "\t\t\t\t" + "Product Name : " + productName + " .\n" +
                            "\t\t\t\t" + "Product Quantity : " + productQuantity + " .\n\n" +
                            "Best Regards\n" + "Inventory Corporation";
                    Intent intent = new Intent(Intent.ACTION_SEND);
                    intent.setType("application/image");
                    intent.putExtra(Intent.EXTRA_EMAIL, new String[]{supplierEmail});
                    intent.putExtra(Intent.EXTRA_SUBJECT, subject);
                    intent.putExtra(Intent.EXTRA_TEXT, orderDetails);
                    intent.putExtra(Intent.EXTRA_STREAM, imageUri);
                    startActivity(Intent.createChooser(intent, "Send Order Mail"));
                }
            }
        });

        saveProductButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (currentUri == null) {
                    insertInventoryProduct();
                } else {
                    showDialogBeforeUpdateProduct();
                }
            }
        });
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        String[] projection = {InventoryEntry._ID,
                InventoryEntry.COLUMN_PRODUCT_NAME,
                InventoryEntry.COLUMN_PRODUCT_QUANTITY,
                InventoryEntry.COLUMN_PRODUCT_QUANTITY_TYPE,
                InventoryEntry.COLUMN_PRODUCT_PRICE,
                InventoryEntry.COLUMN_PRODUCT_PRICE_TYPE,
                InventoryEntry.COLUMN_PRODUCT_NAME,
                InventoryEntry.COLUMN_PRODUCT_SUPPLIER_NAME,
                InventoryEntry.COLUMN_PRODUCT_SUPPLIER_EMAIL,
                InventoryEntry.COLUMN_PRODUCT_SUPPLIER_PHONE,
                InventoryEntry.COLUMN_PRODUCT_SUPPLIER_ADDRESS,
                InventoryEntry.COLUMN_PRODUCT_IMAGE};
        return new CursorLoader(this, currentUri, projection, null, null, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        //After get Values from Cursor We Start To Set Views with the product info to update it.
        setValuesToViewsToUpdate(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        loader.reset();
    }


    private void uploadImageFromGallery() {

        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);

        intent.addCategory(Intent.CATEGORY_OPENABLE);

        //To Get Only Images From Category
        intent.setType("image/*");

        intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION
                | Intent.FLAG_GRANT_WRITE_URI_PERMISSION
                | Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION);

        startActivityForResult(intent, PICK_PRODUCT_IMAGE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        // Receive the request code if match the product request id start get the image Uri
        if (PICK_PRODUCT_IMAGE == requestCode) {
            if (data != null) {
                imageUri = data.getData();

                getContentResolver().takePersistableUriPermission(imageUri,
                        Intent.FLAG_GRANT_READ_URI_PERMISSION);
                /**
                 * Start Set The Image Bitmap By Uri Of the Image
                 * Using {@link UploadImageBitmap#convertImageUriByBitmap(Uri, Context)}  }
                 */

                ViewTreeObserver viewTreeObserver = productImageView.getViewTreeObserver();
                viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        productImageView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                        productImageView.setImageBitmap(UploadImageBitmap.getBitmapFromUri(imageUri, getApplicationContext(),productImageView));
                    }
                });
              //  productImageView.setImageBitmap(UploadImageBitmap.convertImageUriByBitmap(imageUri, this));

            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void setUpQuantitySpinner() {
        ArrayAdapter spinnerArrayAdapter = ArrayAdapter.createFromResource(this, R.array.quantity_types,
                android.R.layout.simple_spinner_item);

        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);

        spinnerQuantityTypeView.setAdapter(spinnerArrayAdapter);

        spinnerQuantityTypeView.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                String selectedItem = (String) adapterView.getItemAtPosition(position);
                if (!TextUtils.isEmpty(selectedItem)) {
                    if (selectedItem.equals(getString(R.string.quantity_type_piece))) {
                        productQuantityType = InventoryEntry.QUANTITY_TYPE_PEACE;
                    } else {
                        productQuantityType = InventoryEntry.QUANTITY_TYPE_KG;
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void setUpPriceSpinner() {
        ArrayAdapter spinnerPriceArrayAdapter = ArrayAdapter.createFromResource(this,
                R.array.price_types, android.R.layout.simple_spinner_item);

        spinnerPriceArrayAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);

        spinnerPriceTypeView.setAdapter(spinnerPriceArrayAdapter);

        spinnerPriceTypeView.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                String selectedItem = (String) adapterView.getItemAtPosition(position);
                if (!TextUtils.isEmpty(selectedItem)) {
                    if (selectedItem.equals(getString(R.string.price_type_usd))) {
                        productPriceType = InventoryEntry.PRICE_TYPE_USD;
                    } else if (selectedItem.equals(getString(R.string.price_type_euro))) {
                        productPriceType = InventoryEntry.PRICE_TYPE_EURO;
                    } else if (selectedItem.equals(getString(R.string.price_type_kwd))) {
                        productPriceType = InventoryEntry.PRICE_TYPE_KWD;
                    } else if (selectedItem.equals(getString(R.string.price_type_gbp))) {
                        productPriceType = InventoryEntry.PRICE_TYPE_GBP;
                    } else if (selectedItem.equals(getString(R.string.price_type_swiss_franc))) {
                        productPriceType = InventoryEntry.PRICE_TYPE_SWISS_FRANK;
                    } else {
                        productPriceType = InventoryEntry.PRICE_TYPE_YEN;
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    /**
     * in this method {@link #getInventoryValuesForUpdate(Cursor)} :
     * we get values from cursor using helper methods like :
     * {@link #getStringFromCursor(Cursor, String)}
     * {@link #getIntFromCursor(Cursor, String)}
     */
    private void getInventoryValuesForUpdate(Cursor cursor) {
        if (cursor.moveToFirst()) {
            productName = getStringFromCursor(cursor, InventoryEntry.COLUMN_PRODUCT_NAME);
            productQuantity = getIntFromCursor(cursor, InventoryEntry.COLUMN_PRODUCT_QUANTITY);
            productQuantityType = getIntFromCursor(cursor, InventoryEntry.COLUMN_PRODUCT_QUANTITY_TYPE);
            productPrice = getIntFromCursor(cursor, InventoryEntry.COLUMN_PRODUCT_PRICE);
            productPriceType = getIntFromCursor(cursor, InventoryEntry.COLUMN_PRODUCT_PRICE_TYPE);
            supplierName = getStringFromCursor(cursor, InventoryEntry.COLUMN_PRODUCT_SUPPLIER_NAME);
            supplierEmail = getStringFromCursor(cursor, InventoryEntry.COLUMN_PRODUCT_SUPPLIER_EMAIL);
            supplierPhone = getStringFromCursor(cursor, InventoryEntry.COLUMN_PRODUCT_SUPPLIER_PHONE);
            supplierAddress = getStringFromCursor(cursor, InventoryEntry.COLUMN_PRODUCT_SUPPLIER_ADDRESS);
            imageUri = Uri.parse(getStringFromCursor(cursor, InventoryEntry.COLUMN_PRODUCT_IMAGE));
        }
    }

    /**
     * After Get Values from cursor using this method :
     * {@link #getInventoryValuesForUpdate(Cursor)}
     * We Start Set Received Values to views to let user update the product Values
     */
    private void setValuesToViewsToUpdate(Cursor cursor) {

        getInventoryValuesForUpdate(cursor);

        productNameView.setText(productName);
        productQuantityView.setText(String.valueOf(productQuantity));

        switch (productQuantityType) {
            case InventoryEntry.QUANTITY_TYPE_KG:
                spinnerQuantityTypeView.setSelection(1);
                break;
            default:
                spinnerQuantityTypeView.setSelection(0);
        }

        productPriceView.setText(String.valueOf(productPrice));

        switch (productPriceType) {
            case InventoryEntry.PRICE_TYPE_EURO:
                spinnerPriceTypeView.setSelection(1);
                break;
            case InventoryEntry.PRICE_TYPE_KWD:
                spinnerPriceTypeView.setSelection(2);
                break;
            case InventoryEntry.PRICE_TYPE_GBP:
                spinnerPriceTypeView.setSelection(3);
                break;
            case InventoryEntry.PRICE_TYPE_SWISS_FRANK:
                spinnerPriceTypeView.setSelection(4);
                break;
            case InventoryEntry.PRICE_TYPE_YEN:
                spinnerPriceTypeView.setSelection(5);
                break;
            default:
                spinnerPriceTypeView.setSelection(0);
                break;
        }
        supplierNameView.setText(supplierName);
        supplierEmailView.setText(supplierEmail);
        supplierPhoneView.setText(supplierPhone);
        supplierAddressView.setText(supplierAddress);

        productImageView.setImageBitmap(UploadImageBitmap.convertImageUriByBitmap(imageUri, this));
    }

    //get values from views after user Entered them .
    private void getValuesFromViews() {
        productName = productNameView.getText().toString().trim();

        if (productQuantityView.getText().toString().matches("")) {
            productQuantity = 0;
        } else {
            productQuantity = Integer.parseInt(productQuantityView.getText().toString());
        }

        if (productPriceView.getText().toString().matches("")) {
            productPrice = 0;
        } else {
            productPrice = Integer.parseInt(productPriceView.getText().toString());
        }
        supplierName = supplierNameView.getText().toString().trim();
        supplierEmail = supplierEmailView.getText().toString().trim();
        supplierPhone = supplierPhoneView.getText().toString().trim();
        supplierAddress = supplierAddressView.getText().toString().trim();
    }

    private ContentValues getContentValuesColumns() {

        ContentValues contentValues = new ContentValues();
        contentValues.put(InventoryEntry.COLUMN_PRODUCT_NAME, productName);
        contentValues.put(InventoryEntry.COLUMN_PRODUCT_QUANTITY, productQuantity);
        contentValues.put(InventoryEntry.COLUMN_PRODUCT_QUANTITY_TYPE, productQuantityType);
        contentValues.put(InventoryEntry.COLUMN_PRODUCT_PRICE, productPrice);
        contentValues.put(InventoryEntry.COLUMN_PRODUCT_PRICE_TYPE, productPriceType);
        contentValues.put(InventoryEntry.COLUMN_PRODUCT_SUPPLIER_NAME, supplierName);
        contentValues.put(InventoryEntry.COLUMN_PRODUCT_SUPPLIER_EMAIL, supplierEmail);
        contentValues.put(InventoryEntry.COLUMN_PRODUCT_SUPPLIER_PHONE, supplierPhone);
        contentValues.put(InventoryEntry.COLUMN_PRODUCT_SUPPLIER_ADDRESS, supplierAddress);
        contentValues.put(InventoryEntry.COLUMN_PRODUCT_IMAGE, imageUri.toString());

        return contentValues;
    }

    private void deleteInventoryItem() {

        int rowDeleted = 0;
        if (currentUri != null) {
            rowDeleted = getContentResolver().delete(currentUri, null, null);
        }

        if (rowDeleted == 0) {
            makeToast(getString(R.string.cant_delete_this_product));
        } else {
            makeToast(getString(R.string.product_deleted_correctly));
        }

        //After Delete product finish the Current Activity
        finish();
    }

    private void updateInventoryItem() {

        getValuesFromViews();

        //First Start Make Validation For Values Before Update Them
        if (!validateName(productName)) {
            validateNameMessages(productName);
        } else if (productQuantity == 0) {
            makeToast(getString(R.string.quantity_cannot_be_zero));
        } else if (productPrice == 0) {
            makeToast(getString(R.string.price_cannot_be_empty));
        } else if (!validateName(supplierName)) {
            validateNameMessages(supplierName);
        } else if (!validateSupplierEmail(supplierEmail)) {
            validateEmailMessage(supplierEmail);
        } else if (TextUtils.isEmpty(supplierPhone)) {
            makeToast(getString(R.string.phone_cannot_be_null));
        } else if (imageUri == null) {
            makeToast(getString(R.string.product_must_have_image));
        } else {

            ContentValues contentValues = getContentValuesColumns();

            int rowUpdated = getContentResolver().update(currentUri, contentValues, null, null);

            if (rowUpdated == 0) {
                makeToast(getString(R.string.failed_to_update_this_product) + rowUpdated);
            } else {
                makeToast(getString(R.string.product_updated_correctly));
            }

            //After Update the product finish the Current Activity
            finish();
        }
    }

    private void insertInventoryProduct() {
        //get Values From Views To Start Insert Them Into The Database
        getValuesFromViews();

        //First Start Make Validation For Values Before Insert Them
        if (!validateName(productName)) {
            validateNameMessages(productName);
        } else if (productQuantity == 0) {
            makeToast(getString(R.string.quantity_cannot_be_zero));
        } else if (productPrice == 0) {
            makeToast(getString(R.string.price_cannot_be_empty));
        } else if (!validateName(supplierName)) {
            validateNameMessages(supplierName);
        } else if (!validateSupplierEmail(supplierEmail)) {
            validateEmailMessage(supplierEmail);
        } else if (TextUtils.isEmpty(supplierPhone)) {
            makeToast(getString(R.string.phone_cannot_be_null));
        } else if (imageUri == null) {
            makeToast(getString(R.string.product_must_have_image));
        } else {

            ContentValues contentValues = getContentValuesColumns();

            Uri newUri = getContentResolver().insert(InventoryEntry.CONTENT_URI, contentValues);

            if (newUri == null) {
                makeToast(getString(R.string.failed_to_insert_product));
            } else {
                makeToast(getString(R.string.product_saved_correctly));
            }

            finish();
        }
    }

    private void showDialogBeforeBackPressed() {

        DialogInterface.OnClickListener positiveDialog = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        };

        DialogInterface.OnClickListener negativeDialog = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        };

        DisplayAlertDialog.showAlertDialog(this,
                getString(R.string.discard_changes_message),
                getString(R.string.discard),
                positiveDialog,
                getString(R.string.cancel),
                negativeDialog);
    }

    private void showDialogBeforeDeleteProduct() {

        DialogInterface.OnClickListener positiveDialog = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                deleteInventoryItem();
            }
        };

        DialogInterface.OnClickListener negativeDialog = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        };

        DisplayAlertDialog.showAlertDialog(this,
                getString(R.string.delete_message),
                getString(R.string.delete),
                positiveDialog,
                getString(R.string.cancel),
                negativeDialog);
    }

    private void showDialogBeforeUpdateProduct() {

        DialogInterface.OnClickListener positiveDialog = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                updateInventoryItem();
            }
        };
        DialogInterface.OnClickListener negativeDialog = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        };
        DisplayAlertDialog.showAlertDialog(this,
                getString(R.string.update_message),
                getString(R.string.update), positiveDialog,
                getString(R.string.cancel), negativeDialog);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.editor_activty_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        switch (itemId) {
            case R.id.save_item_icon_editor_activity:
                if (currentUri == null) {
                    insertInventoryProduct();
                } else {
                    showDialogBeforeUpdateProduct();
                }
                return true;
            case R.id.delete_item_icon_editor_activity:
                showDialogBeforeDeleteProduct();
                return true;
            case android.R.id.home:
                if (!productHasChanged) {
                    NavUtils.navigateUpFromSameTask(EditActivity.this);
                } else {
                    showDialogBeforeBackPressed();
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);

        if (currentUri == null) {
            MenuItem menuItem = menu.findItem(R.id.delete_item_icon_editor_activity);
            menuItem.setVisible(false);
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        if (!productHasChanged) {
            super.onBackPressed();
            return;
        }

        showDialogBeforeBackPressed();
    }

    //Helper Methods
    private String getStringFromCursor(Cursor cursor, String columnName) {
        return cursor.getString(cursor.getColumnIndexOrThrow(columnName));
    }

    private int getIntFromCursor(Cursor cursor, String columnName) {
        return cursor.getInt(cursor.getColumnIndexOrThrow(columnName));
    }

    private void makeToast(String text) {
        Toast.makeText(getApplicationContext(), text, Toast.LENGTH_SHORT).show();
    }

    //Check Name if Valid Or not
    private boolean validateName(String name) {
        if (TextUtils.isEmpty(name)) {
            return false;
        } else if (name.length() < 4) {
            return false;
        } else {
            return true;
        }
    }

    //Display Message Of Invalid Case
    private void validateNameMessages(String name) {
        if (TextUtils.isEmpty(name)) {
            makeToast("Name cannot be empty");
        } else if (name.length() < 4) {
            makeToast("the name cannot be less than 4 Characters");
        }
    }

    //Display Message Of Invalid Case
    private void validateSupplierNameMessages(String name) {
        if (TextUtils.isEmpty(name)) {
            makeToast("Supplier Name cannot be empty");
        } else if (name.length() < 4) {
            makeToast("the name cannot be less than 4 Characters");
        }
    }

    //Check Email if Valid Or not
    private boolean validateSupplierEmail(String email) {
        if (email.contains("@") && email.contains(".com") && email.length() > 12) {
            return true;
        } else {
            return false;
        }
    }

    //Display Message Of Invalid Case
    private void validateEmailMessage(String email) {
        if (!email.contains("@")) {
            makeToast("your mail doesn't have \"@\" , type correct one !!");
        } else if (!email.contains(".com")) {
            makeToast("your email doesn't have \".com\" , type correct one");
        } else if (email.length() <= 12) {
            makeToast("the basic info of mail from 10 to 12 , So yo entered invalid Email");
        }
    }

}
